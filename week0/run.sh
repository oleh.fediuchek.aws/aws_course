#!/bin/sh

main() {

  DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

  STACK_NAME=$1

  if [ -z ${DEFAULT_EC2_DEPLOY+x} ] && [ "${DEFAULT_EC2_DEPLOY}" = true ]; then
    echo "DEFAULT_EC2_DEPLOY is unset, use custom setup"

     STACK_ID=$(aws cloudformation create-stack --template-body file://"${DIR}"/ec2setup.yaml --stack-name "$STACK_NAME" \
     --parameters file://"${DIR}"/configuration.json | jq -r .StackId)

     waitStack
  else
    echo "DEFAULT_EC2_DEPLOY is set to '$DEFAULT_EC2_DEPLOY'"

    echo "    # Deploy by default
    # InstanceTypeParam: t2.micro
    # AmiIdParam: ami-0c2d06d50ce30b442 (Linux x64)
    # AvailabilityZoneParam: us-west-2
    # SSHKeyNameParam: aws_course_default
    # SecurityGroupSSHAllowedIpParam: 0.0.0.0/0"

    STACK_ID=$(aws cloudformation create-stack --template-body file://"${DIR}"/ec2setup.yaml --stack-name "$STACK_NAME" | jq -r .StackId)

    waitStack
  fi

}

waitStack() {
  aws cloudformation wait stack-create-complete --stack-name "$STACK_ID"
  aws cloudformation describe-stacks --stack-name "$STACK_ID" | jq .Stacks[0].Outputs
}

main "$@"
