# HOW TO DEPLOY EC2 via Cloudformation

## Deploy

### Requirements 
* jq installed
* Generated/imported keys from AWS EC2 console with a default name "aws_course_default", or you can choose another name
* AWS user credentials in your default aws profile / or in environment variables: 
```
Example profile configuration: 

~/.aws/credentials

[aws_course_root]
aws_access_key_id = AKIA####
aws_secret_access_key = n6iv0####

Example environment configuration:
export AWS_ACCESS_KEY_ID=AKIA####
export AWS_SECRET_ACCESS_KEY=wJalr####
export AWS_DEFAULT_REGION=us-west-2

```

### You have 2 possible ways to deploy that: 
1. Using the default/preconfigured variables:
   ```
    # Deploy by default
    # InstanceTypeParam: t2.micro
    # AmiIdParam: ami-0c2d06d50ce30b442 (Linux x64)
    # AvailabilityZoneParam: us-west-2
    # SSHKeyNameParam: aws_course_default
    # SecurityGroupSSHAllowedIpParam: 0.0.0.0/0
   
   export DEFAULT_EC2_DEPLOY=true
   
   sh run.sh "your_stack_name"
   e.g
   sh run.sh week0
   ```
2. Using your personal configuration:
   ```
   # if any variables is missing, it will be replaced with the default one
   
   Check the ./configuration.yaml and update properties
   
   sh run.sh "your_stack_name"
   ```
   
### How to destroy the stack:
```
sh destroy.sh "your_stack_name"
e.g 
sh destroy.sh week0
```